//////////////////  photo slider  ///////////////////////

window.addEventListener("load", function() {

    let buttonPrev = document.querySelector('.usecases-screen__button-prev');
    let buttonNext = document.querySelector('.usecases-screen__button-next');

    let slides = document.querySelectorAll('.usecases-screen__slide');
    let i = 0;


    buttonPrev.onclick = function () {
        slides[i].style.display = 'none';
        i--;

        if(i < 0){
            i = slides.length - 1;
        }

        slides[i].style.display = 'block';

    };

    buttonNext.onclick = function () {
        slides[i].style.display = 'none';
        i++;

        if(i >= slides.length){
            i = 0;
        }

        slides[i].style.display = 'block';
    };

    //////////////////  mobile menu  ///////////////////////

    const openMenuButton = document.querySelector('.show-menu-btn');
    const closeMenuButton = document.querySelector('.hide-menu-btn');
    const menuBlock = document.querySelector('.main-nav__menu');
    const menuLink = document.querySelectorAll('.main-nav__link');

    openMenuButton.addEventListener('click', showMobileMenu);
    closeMenuButton.addEventListener('click', closeMobileMenu);
    [...menuLink].map(el => el.addEventListener('click', closeMobileMenu));

    function showMobileMenu() {
        menuBlock.classList.add('main-nav__menu_show');
        closeMenuButton.style.display = 'block';
    }

    function closeMobileMenu() {
        menuBlock.classList.remove('main-nav__menu_show');
        closeMenuButton.style.display = 'none';
    }


    //////////////////  sticky menu  ///////////////////////

    document.addEventListener('scroll', stickyMenu);

    let navbar = document.querySelector(".main-nav");

    // Get the offset position of the navbar
    let sticky = navbar.offsetTop;

    // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position

    function stickyMenu () {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add('sticky-menu')
        } else {
            navbar.classList.remove('sticky-menu')
        }
    }


    //////////////////  sign-up__form  ///////////////////////
    const signUpForm = document.querySelector('.sign-up-form');
    const signUpButtons = document.querySelectorAll('.main-nav__register-link');
    const signUpFormCloseButton = document.querySelector('.sign-up-form__close-btn');

    for (const button of signUpButtons) {
        button.addEventListener('click', showSignUpForm);
    }

    function showSignUpForm(e) {
            e.preventDefault();
            signUpForm.classList.add('sign-up-form_show');
    }

    signUpFormCloseButton.addEventListener('click', closeSignUpForm);

    function closeSignUpForm(e) {
        e.preventDefault();
        signUpForm.classList.remove('sign-up-form_show')
    }

    //////////////////  log-in__form  ///////////////////////
    const logInForm = document.querySelector('.log-in-form');
    const logInButtons = document.querySelectorAll('.main-nav__enter-link');
    const logInFormCloseButton = document.querySelector('.log-in-form__close-btn');

    for (const button of logInButtons){
        button.addEventListener('click', showLogInForm);
    }

    function showLogInForm(e) {
        e.preventDefault();
        logInForm.classList.add('log-in-form_show')
    }

    logInFormCloseButton.addEventListener('click', closeLogInForm);

    function closeLogInForm(e) {
        e.preventDefault();
        logInForm.classList.remove('log-in-form_show')
    }
});